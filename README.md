# Shockbyte backend test

## Installation

First clone the project

```shell
git clone <repo_link>
```

Then you should install the packages:

```shell
npm install
```

Copy .env.example to create .env file (the values are filled since it's a test and there's no sesitive data)

```shell
cp .env.example .env
```


Then let's seed some data:

```shell
npm run seed
```

run the project in development mode:

```shell
npm run start:dev
```

run the tests

```shell
npm run test
```
I hope everything is Green!

## Structure

Let's explain the structure briefly
- common

Here we put everything that might been used all over the app, constants, exceptions, helpers, etc ..

- config

Here we put the config for the modules, database config, external API keys, etc ..

- jobs

Here we can put cron jobs, Scheduled Tasks, job queues, etc ..

- Modules

Here we can put the main features and the business logic for our project, each feature is mainly consist of (controller , service , module)
and we encapsulate everything related to this feature within it's own folder

- Providers

Here we can put the providers for our project that will be used by our modules

## Things that I could improve more

- I could add unit tests for the controller layer also
- More abstraction to the code (but I didn't want to over-engineer the project)
- Make an API documentation using Swagger or other tools
- Use Faker to generate more realistic Data for seeding

## Thank You!

And finally I want to express my sincere gratitude for providing me with the opportunity to work on this test




