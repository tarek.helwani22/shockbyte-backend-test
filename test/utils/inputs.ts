import { Birdhouse } from '@/modules/birdhouse/types/birdhouse.type';

export const birdhouseCreateInput: Birdhouse = {
  name: 'Test Birdhouse',
  latitude: 98.1,
  longitude: 13.1,
};
