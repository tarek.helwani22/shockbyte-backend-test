import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppConfigModule } from "./app/app-config.module";
import { MongoConfigModule } from "./database/mongo/mongo-config.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env'],
      isGlobal: true,
      cache: true,
    }),
    AppConfigModule,
    MongoConfigModule,
  ],
})
export class ConfigsModule {}
