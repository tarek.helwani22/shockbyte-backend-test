import { Injectable } from '@nestjs/common';
import { BaseConfigService } from "../base-config.service";
import { EnvTypes } from "./types/enviroment";

@Injectable()
export class AppConfigService extends BaseConfigService {
  get name(): string {
    return this.getValue<string>('app.name');
  }
  get env(): EnvTypes {
    return this.getValue<EnvTypes>('app.env');
  }
  get url(): string {
    return this.getValue<string>('app.url');
  }
  get port(): number {
    return this.getValue<number>('app.port');
  }
}
