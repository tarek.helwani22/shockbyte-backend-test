import * as Joi from '@hapi/joi';
import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppConfigService } from './app-config.service';
import configuration from './app-configuration';
import { EnvTypes } from "./types/enviroment";

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      load: configuration,
      validationSchema: Joi.object({
        APP_NAME: Joi.string().default('MyApp'),
        APP_ENV: Joi.string()
          .valid(
            ...[EnvTypes.PRODUCTION, EnvTypes.DEVELOPMENT, EnvTypes.STAGING],
          )
          .default(EnvTypes.DEVELOPMENT),
        APP_URL: Joi.string().default('http://localhost:3000/'),
        APP_PORT: Joi.number().default(3000),
      }),
    }),
  ],
  providers: [ConfigService, AppConfigService],
  exports: [ConfigService, AppConfigService],
})
export class AppConfigModule {}
