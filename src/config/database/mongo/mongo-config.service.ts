import { Injectable } from '@nestjs/common';
import { ConfigService as NestConfigService } from '@nestjs/config/dist/config.service';

@Injectable()
export class MongoConfigService {
  constructor(private readonly nestConfigService: NestConfigService) {}

  get port(): number {
    return Number(this.nestConfigService.get<number>('db.port'));
  }
  get host(): string {
    return this.nestConfigService.get<string>('db.host');
  }
  get username(): string {
    return this.nestConfigService.get<string>('db.username');
  }
  get password(): string {
    return this.nestConfigService.get<string>('db.password');
  }
  get database(): string {
    return this.nestConfigService.get<string>('db.database');
  }
  get synchronize(): boolean {
    return this.nestConfigService.get<boolean>('db.synchronize');
  }
  get url(): string {
    return this.nestConfigService.get<string>('db.url');
  }
}
