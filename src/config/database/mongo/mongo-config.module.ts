import * as Joi from '@hapi/joi';
import { Module } from '@nestjs/common';
import configuration from './mongo-configuration';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongoConfigService } from "./mongo-config.service";


@Module({
  imports: [
    ConfigModule.forRoot({
      load: configuration,
      validationSchema: Joi.object({
        DB_PORT: Joi.number().default(27017),
        DB_HOST: Joi.string().default('localhost'),
        DB_USERNAME: Joi.string().default('root'),
        DB_PASSWORD: Joi.string().allow(''),
        DB_DATABASE: Joi.optional(),
        DB_SYNCHRONIZE: Joi.boolean().default(false),
        DB_URL: Joi.string().uri(),
      }),
    }),
  ],
  providers: [ConfigService, MongoConfigService],
  exports: [ConfigService, MongoConfigService],
})
export class MongoConfigModule {}