import { registerAs } from '@nestjs/config';
export default [
  registerAs('db', () => ({
    port: process.env.DB_PORT,
    host: process.env.DB_HOST,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    synchronize: process.env.DB_SYNCHRONIZE,
    url: process.env.DB_URL,
  })),
]

