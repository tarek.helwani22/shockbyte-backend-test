import { Module } from '@nestjs/common';
import { MongooseProviderModule } from '@/providers/database/mongoose/mongoose-provider.module';

@Module({
  imports: [MongooseProviderModule],
})
export class CronModule {}
