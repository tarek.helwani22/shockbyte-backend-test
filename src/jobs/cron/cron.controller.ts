import { Controller } from '@nestjs/common';
import { MongoDataService } from '@/providers/database/mongoose/mongo-data.service';

import { Cron, CronExpression } from '@nestjs/schedule';

@Controller('cron')
export class CronController {
  constructor(private readonly mongoDataServices: MongoDataService) {}

  @Cron(CronExpression.EVERY_YEAR)
  async pruneBirdhousesNotUpdatedInAYear(): Promise<void> {
    const oneYearAgo = new Date();
    oneYearAgo.setFullYear(oneYearAgo.getFullYear() - 1);

    try {
      const birdhousesToDelete = await this.mongoDataServices.birdhouses
        .find({
          lastUpdated: { $lt: oneYearAgo },
        })
        .exec();

      await Promise.all(
        birdhousesToDelete.map((birdhouse) => birdhouse.deleteOne()),
      );
    } catch (error) {
      console.error('Error pruning birdhouses:', error);
    }
  }
}
