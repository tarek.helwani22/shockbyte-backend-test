/**
 * Module for registering global providers.
 * Only add global providers to this module.
 */
import { Module } from "@nestjs/common";
import { MongooseProviderModule } from "./database/mongoose/mongoose-provider.module";

@Module({
  imports: [
    MongooseProviderModule
  ],
  exports: [],
})
export class ProvidersModule {}
