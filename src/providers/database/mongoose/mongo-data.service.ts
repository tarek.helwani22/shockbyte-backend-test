import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BirdhouseDocument } from '@/modules/birdhouse/schemas/birdhouse.schema';

@Injectable()
export class MongoDataService implements OnApplicationBootstrap {
  birdhouses: Model<BirdhouseDocument>;

  constructor(
    @InjectModel(BirdhouseDocument.name)
    private birdhouseRepository: Model<BirdhouseDocument>,
  ) {}

  onApplicationBootstrap() {
    this.birdhouses = this.birdhouseRepository;
  }
}
