import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MongoConfigModule } from '@/config/database/mongo/mongo-config.module';
import { MongoConfigService } from '@/config/database/mongo/mongo-config.service';
import {
  BirdhouseDocument,
  BirdhouseSchema,
} from '@/modules/birdhouse/schemas/birdhouse.schema';
import { MongoDataService } from './mongo-data.service';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [MongoConfigModule],
      inject: [MongoConfigService],
      useFactory: async (mongoConfigService: MongoConfigService) => ({
        uri: mongoConfigService.url,
      }),
    }),

    MongooseModule.forFeature([
      { name: BirdhouseDocument.name, schema: BirdhouseSchema },
    ]),
  ],
  providers: [MongoDataService],
  exports: [MongoDataService],
})
export class MongooseProviderModule {}
