import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { BirdhouseService } from '../birdhouse.service';

@Injectable()
export class UbidGuard implements CanActivate {
  constructor(private readonly birdhouseService: BirdhouseService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const ubidHeader = request.header('X-UBID');

    if (!ubidHeader) {
      throw new HttpException(
        'X-UBID header is missing',
        HttpStatus.UNAUTHORIZED,
      );
    }

    const birdhouse = await this.birdhouseService.findByUbid(ubidHeader);

    if (!birdhouse) {
      throw new HttpException('Invalid X-UBID', HttpStatus.FORBIDDEN);
    }

    return true;
  }
}
