import { IsString, IsNumber, MinLength, MaxLength } from 'class-validator';

export class CreateBirdhouseDto {
  @IsNumber()
  longitude: number;

  @IsNumber()
  latitude: number;

  @IsString()
  @MinLength(4, { message: 'Name must be at least 4 characters long' })
  @MaxLength(16, {
    message: 'Name must be less or equal than 16 characters long',
  })
  name: string;
}
