import { IsOptional } from 'class-validator';

export class GetResidencyDataDto {
  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;
}
