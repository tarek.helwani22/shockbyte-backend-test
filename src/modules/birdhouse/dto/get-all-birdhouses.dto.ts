import { IsOptional } from 'class-validator';

export class GetAllBirdhousesDto {
  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;
}
