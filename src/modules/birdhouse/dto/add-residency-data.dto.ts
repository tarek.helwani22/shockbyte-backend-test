import { IsNumber } from 'class-validator';

export class AddResidencyDataDto {
  @IsNumber()
  birds: number;

  @IsNumber()
  eggs: number;
}
