import {
  IsString,
  IsNumber,
  MinLength,
  MaxLength,
  IsOptional,
} from 'class-validator';

export class UpdateBirdhouseDto {
  @IsNumber()
  @IsOptional()
  longitude?: number;

  @IsNumber()
  @IsOptional()
  latitude?: number;

  @IsString()
  @MinLength(4, { message: 'Name must be at least 4 characters long' })
  @MaxLength(16, {
    message: 'Name must be less or equal than 16 characters long',
  })
  @IsOptional()
  name?: string;
}
