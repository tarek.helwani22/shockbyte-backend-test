export interface ResidencyData {
  birds: number;
  eggs: number;
}

export type ResidencyDataWithTimestamp = ResidencyData & {
  timestamp: Date;
};
