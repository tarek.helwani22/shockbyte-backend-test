export interface Birdhouse {
  name: string;
  longitude: number;
  latitude: number;
}
