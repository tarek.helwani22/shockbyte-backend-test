export interface GetPaginatedResidencyDataInput {
  id: string;
  page: number;
  limit: number;
}

export interface GetPaginatedResidencyDataResult {
  total: number;
  limit: number;
  data: {
    birds: number;
    eggs: number;
    timestamp: Date;
  }[];
}
