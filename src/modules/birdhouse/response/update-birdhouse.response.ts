export interface UpdateBirdhouseResponse {
  birds: number;
  eggs: number;
  longitude: number;
  latitude: number;
  name: string;
}
