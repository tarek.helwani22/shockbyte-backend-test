export interface GetBirdhousesResponse {
  id: string;
  birds: number;
  eggs: number;
  longitude: number;
  latitude: number;
  name: string;
}
