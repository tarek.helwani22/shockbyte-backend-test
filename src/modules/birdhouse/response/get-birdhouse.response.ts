export interface GetBirdhouseResponse {
  longitude: number;
  latitude: number;
  name: string;
}
