export interface CreateBirdhouseResponse {
  id: string;
  ubid: string;
  birds: number;
  eggs: number;
  longitude: number;
  latitude: number;
  name: string;
}
