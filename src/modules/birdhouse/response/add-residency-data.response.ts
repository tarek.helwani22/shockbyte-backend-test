export interface AddResidencyDataResponse {
  birds: number;
  eggs: number;
  longitude: number;
  latitude: number;
  name: string;
}
