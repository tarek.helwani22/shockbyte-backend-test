import { BirdhouseService } from '@/modules/birdhouse/birdhouse.service';
import { BirdhouseController } from '@/modules/birdhouse/birdhouse.controller';
import { Test } from '@nestjs/testing';
import { closeTestDatabase, setupTestDatabase } from '../../../test/test-setup';
import { Birdhouse } from '@/modules/birdhouse/types/birdhouse.type';
import { MongooseProviderModule } from '@/providers/database/mongoose/mongoose-provider.module';
import { Logger } from '@nestjs/common';
import { MongoDataService } from '@/providers/database/mongoose/mongo-data.service';
import { birdhouseCreateInput } from '../../../test/utils/inputs';
import { BirdhouseNotFoundException } from '@/modules/birdhouse/exceptions/birdhouse-not-found.exception';
import * as mongoose from 'mongoose';
import { ResidencyData } from '@/modules/birdhouse/types/residency-data.type';
import { GetResidencyDataDto } from '@/modules/birdhouse/dto/get-residency-data.dto';

describe('BirdhouseService', () => {
  let birdhouseService: BirdhouseService;
  let mongoDataService: MongoDataService;

  beforeAll(async () => {
    await setupTestDatabase();

    const moduleRef = await Test.createTestingModule({
      imports: [MongooseProviderModule],
      controllers: [BirdhouseController],
      providers: [BirdhouseService, Logger],
    }).compile();

    birdhouseService = await moduleRef.resolve(BirdhouseService);
    mongoDataService = await moduleRef.resolve(MongoDataService);

    mongoDataService.onApplicationBootstrap();
  });

  beforeEach(async () => {
    await mongoDataService.birdhouses.deleteMany({});
  });

  afterAll(async () => {
    await closeTestDatabase();
  });

  describe('createBirdhouse', () => {
    it('should create a birdhouse and return it', async () => {
      const createdBirdhouse = await birdhouseService.createBirdhouse(
        birdhouseCreateInput,
      );

      expect(createdBirdhouse).toBeDefined();
      expect(createdBirdhouse.name).toBe(birdhouseCreateInput.name);
      expect(createdBirdhouse.latitude).toBe(birdhouseCreateInput.latitude);
      expect(createdBirdhouse.longitude).toBe(birdhouseCreateInput.longitude);
    });
  });

  describe('updateBirdhouse', () => {
    it('should update a birdhouse and return it', async () => {
      const createdBirdhouse = await birdhouseService.createBirdhouse(
        birdhouseCreateInput,
      );

      const updatedBirdhouseData: Partial<Birdhouse> = {
        name: 'Updated Birdhouse',
      };
      const updatedBirdhouse = await birdhouseService.updateBirdhouse(
        createdBirdhouse.id,
        updatedBirdhouseData,
      );

      expect(updatedBirdhouse).toBeDefined();
      expect(updatedBirdhouse.name).toBe(updatedBirdhouseData.name);
    });

    it('should throw BirdhouseNotFoundException if birdhouse not found', async () => {
      const nonExistentBirdhouseId = new mongoose.Types.ObjectId().toString();

      const updatedBirdhouseData: Partial<Birdhouse> = {
        name: 'Updated Birdhouse',
      };

      await expect(
        birdhouseService.updateBirdhouse(
          nonExistentBirdhouseId,
          updatedBirdhouseData,
        ),
      ).rejects.toThrow(BirdhouseNotFoundException);
    });
  });

  describe('addResidencyData', () => {
    it('should add residency data to a birdhouse and return it', async () => {
      const createdBirdhouse = await birdhouseService.createBirdhouse(
        birdhouseCreateInput,
      );

      const residencyData: ResidencyData = {
        birds: 5,
        eggs: 10,
      };
      const updatedBirdhouse = await birdhouseService.addResidencyData(
        createdBirdhouse.id,
        residencyData,
      );

      expect(updatedBirdhouse).toBeDefined();
      expect(updatedBirdhouse.residencyData).toHaveLength(1);
      expect(updatedBirdhouse.residencyData[0].birds).toBe(residencyData.birds);
      expect(updatedBirdhouse.residencyData[0].eggs).toBe(residencyData.eggs);
    });

    it('should throw BirdhouseNotFoundException if birdhouse not found', async () => {
      const nonExistentBirdhouseId = new mongoose.Types.ObjectId().toString();
      const residencyData: ResidencyData = {
        birds: 5,
        eggs: 10,
      };

      await expect(
        birdhouseService.addResidencyData(
          nonExistentBirdhouseId,
          residencyData,
        ),
      ).rejects.toThrow(BirdhouseNotFoundException);
    });
  });

  describe('getBirdhouseInfo', () => {
    it('should get birdhouse info and return it', async () => {
      const createdBirdhouse = await birdhouseService.createBirdhouse(
        birdhouseCreateInput,
      );

      const retrievedBirdhouse = await birdhouseService.getBirdhouseInfo(
        createdBirdhouse.id,
      );

      expect(retrievedBirdhouse).toBeDefined();
      expect(retrievedBirdhouse.name).toBe(birdhouseCreateInput.name);
      expect(retrievedBirdhouse.latitude).toBe(birdhouseCreateInput.latitude);
      expect(retrievedBirdhouse.longitude).toBe(birdhouseCreateInput.longitude);
    });

    it('should throw BirdhouseNotFoundException if birdhouse not found', async () => {
      const nonExistentBirdhouseId = new mongoose.Types.ObjectId().toString();

      await expect(
        birdhouseService.getBirdhouseInfo(nonExistentBirdhouseId),
      ).rejects.toThrow(BirdhouseNotFoundException);
    });
  });

  describe('getAllBirdhouses', () => {
    it('should get all birdhouses and return an array', async () => {
      const birdhouseData1: Birdhouse = {
        name: 'Birdhouse 1',
        latitude: 98.1,
        longitude: 13.1,
      };
      const birdhouseData2: Birdhouse = {
        name: 'Birdhouse 2',
        latitude: 98.2,
        longitude: 13.2,
      };
      const birdhouseData3: Birdhouse = {
        name: 'Birdhouse 3',
        latitude: 98.3,
        longitude: 13.3,
      };

      await birdhouseService.createBirdhouse(birdhouseData1);
      await birdhouseService.createBirdhouse(birdhouseData2);
      await birdhouseService.createBirdhouse(birdhouseData3);

      const allBirdhouses = await birdhouseService.getAllBirdhouses(0, 10);

      expect(Array.isArray(allBirdhouses)).toBe(true);
      expect(allBirdhouses.length).toBe(3);
    });
  });

  describe('getPaginatedResidencyData', () => {
    it('should retrieve paginated residency data for a birdhouse', async () => {
      const createdBirdhouse = await birdhouseService.createBirdhouse(
        birdhouseCreateInput,
      );

      // Create some residency data for the birdhouse
      const residencyData: ResidencyData[] = [
        { birds: 5, eggs: 10 },
        { birds: 3, eggs: 8 },
        { birds: 8, eggs: 15 },
      ];

      for (const data of residencyData) {
        await birdhouseService.addResidencyData(createdBirdhouse.id, data);
      }

      const getPaginatedResidencyDataDto: GetResidencyDataDto = {
        page: 1,
        limit: 2,
      };

      const paginatedResidencyData =
        await birdhouseService.getPaginatedResidencyData({
          id: createdBirdhouse.id,
          page: getPaginatedResidencyDataDto.page,
          limit: getPaginatedResidencyDataDto.limit,
        });

      expect(paginatedResidencyData).toBeDefined();
      expect(paginatedResidencyData.total).toBe(residencyData.length);
      expect(paginatedResidencyData.limit).toBe(
        getPaginatedResidencyDataDto.limit,
      );
      expect(paginatedResidencyData.data).toHaveLength(
        getPaginatedResidencyDataDto.limit,
      );

      // Verify the data in the paginated result
      const paginatedData = paginatedResidencyData.data;
      for (let i = 0; i < paginatedData.length; i++) {
        expect(paginatedData[i].birds).toBe(residencyData[i].birds);
        expect(paginatedData[i].eggs).toBe(residencyData[i].eggs);
      }
    });

    it('should return empty data if birdhouse has no residency data', async () => {
      const createdBirdhouse = await birdhouseService.createBirdhouse(
        birdhouseCreateInput,
      );

      // Define pagination parameters
      const getPaginatedResidencyDataDto: GetResidencyDataDto = {
        page: 1,
        limit: 2,
      };

      const paginatedResidencyData =
        await birdhouseService.getPaginatedResidencyData({
          id: createdBirdhouse.id,
          page: getPaginatedResidencyDataDto.page,
          limit: getPaginatedResidencyDataDto.limit,
        });

      expect(paginatedResidencyData).toBeDefined();
      expect(paginatedResidencyData.total).toBe(0);
      expect(paginatedResidencyData.limit).toBe(
        getPaginatedResidencyDataDto.limit,
      );
      expect(paginatedResidencyData.data).toHaveLength(0);
    });

    it('should return empty data if birdhouse not found', async () => {
      const nonExistentBirdhouseId = new mongoose.Types.ObjectId().toString();

      const getPaginatedResidencyDataDto: GetResidencyDataDto = {
        page: 1,
        limit: 2,
      };

      await expect(
        birdhouseService.getPaginatedResidencyData({
          id: nonExistentBirdhouseId,
          page: getPaginatedResidencyDataDto.page,
          limit: getPaginatedResidencyDataDto.limit,
        }),
      ).rejects.toThrow(BirdhouseNotFoundException);
    });
  });
});
