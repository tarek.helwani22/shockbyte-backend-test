import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { BirdhouseService } from './birdhouse.service';
import { CreateBirdhouseDto } from './dto/create-birdhouse.dto';
import { CreateBirdhouseResponse } from './response/create-birdhouse.response';
import { UpdateBirdhouseDto } from './dto/update-birdhouse.dto';
import { UpdateBirdhouseResponse } from './response/update-birdhouse.response';
import { GetBirdhousesResponse } from './response/get-birdhouses.response';
import { BirdhouseDocument } from './schemas/birdhouse.schema';
import { GetBirdhouseResponse } from './response/get-birdhouse.response';
import { AddResidencyDataDto } from './dto/add-residency-data.dto';
import { AddResidencyDataResponse } from './response/add-residency-data.response';
import { UbidGuard } from './guards/ubid.guard';
import { PaginatedData } from '@/common/types/response';
import { GetResidencyDataDto } from '@/modules/birdhouse/dto/get-residency-data.dto';
import { ResidencyData } from '@/modules/birdhouse/types/residency-data.type';
import { GetPaginatedResidencyDataResult } from '@/modules/birdhouse/defs/birdhouse-service.defs';

@Controller('birdhouses')
export class BirdhouseController {
  constructor(private readonly birdhouseService: BirdhouseService) {}

  /**
   * Create a new birdhouse record.
   * @param createBirdhouseDto The data to create the birdhouse with, including its properties.
   * @returns A newly created birdhouse record with a 201 (Created) status.
   * @throws HttpException with a 500 (Internal Server Error) status if an error occurs during creation.
   */
  @Post()
  @HttpCode(HttpStatus.CREATED)
  async createBirdhouse(
    @Body() createBirdhouseDto: CreateBirdhouseDto,
  ): Promise<CreateBirdhouseResponse> {
    const birdhouseDocument = await this.birdhouseService.createBirdhouse(
      createBirdhouseDto,
    );

    return {
      id: birdhouseDocument.ubid,
      ubid: birdhouseDocument.ubid,
      longitude: birdhouseDocument.longitude,
      latitude: birdhouseDocument.latitude,
      name: birdhouseDocument.name,
      birds: 0,
      eggs: 0,
    };
  }

  /**
   * Update an existing birdhouse record by its ID.
   * @param id The unique identifier (ID) of the birdhouse to update.
   * @param updateBirdhouseDto The data to update the birdhouse with, including its properties.
   * @returns An updated birdhouse record with a 200 (OK) status.
   */
  @UseGuards(UbidGuard)
  @Patch(':id')
  async updateBirdhouse(
    @Param('id') id: string,
    @Body() updateBirdhouseDto: UpdateBirdhouseDto,
  ): Promise<UpdateBirdhouseResponse> {
    const birdhouseDocument = await this.birdhouseService.updateBirdhouse(
      id,
      updateBirdhouseDto,
    );

    const { totalBirds, totalEggs } =
      this.birdhouseService.calculateTotalBirdsAndEggs(birdhouseDocument);

    return {
      longitude: birdhouseDocument.longitude,
      latitude: birdhouseDocument.latitude,
      name: birdhouseDocument.name,
      birds: totalBirds,
      eggs: totalEggs,
    };
  }

  /**
   * Add residency data to a specific birdhouse by its ID.
   * @param {string} id - The unique ID of the birdhouse.
   * @param {AddResidencyDataDto} addResidencyDataDto - The residency data to add.
   * @returns {Promise<AddResidencyDataResponse>} A promise that resolves to the updated birdhouse information.
   */
  @UseGuards(UbidGuard)
  @Post(':id/residency')
  @HttpCode(HttpStatus.CREATED)
  async addResidencyData(
    @Param('id') id: string,
    @Body() addResidencyDataDto: AddResidencyDataDto,
  ): Promise<AddResidencyDataResponse> {
    const birdhouse = await this.birdhouseService.addResidencyData(
      id,
      addResidencyDataDto,
    );

    if (!birdhouse) {
      throw new HttpException('Birdhouse not found', HttpStatus.NOT_FOUND);
    }

    const { totalBirds, totalEggs } =
      this.birdhouseService.calculateTotalBirdsAndEggs(birdhouse);

    return {
      birds: totalBirds,
      eggs: totalEggs,
      longitude: birdhouse.longitude,
      latitude: birdhouse.latitude,
      name: birdhouse.name,
    };
  }

  /**
   * Retrieves information about a specific birdhouse by its ID.
   * @param {string} id - The unique ID of the birdhouse.
   * @returns {Promise<GetBirdhouseResponse>} A promise that resolves to the birdhouse information.
   */
  @UseGuards(UbidGuard)
  @Get(':id')
  async getBirdhouseInfo(
    @Param('id') id: string,
  ): Promise<GetBirdhouseResponse> {
    const birdhouse = await this.birdhouseService.getBirdhouseInfo(id);

    if (!birdhouse) {
      throw new HttpException('Birdhouse not found', HttpStatus.NOT_FOUND);
    }

    return {
      longitude: birdhouse.longitude,
      latitude: birdhouse.latitude,
      name: birdhouse.name,
    };
  }

  /**
   * Retrieves information about all birdhouses with pagination support.
   * @param {number} page - The page number (optional, default: 1).
   * @param {number} limit - The number of items per page (optional, default: 10).
   * @returns {Promise<PaginatedData<GetBirdhousesResponse[]>>} A promise that resolves to paginated data of Birdhouse objects.
   */
  @Get()
  async getAllBirdhouses(
    @Query('page', new ParseIntPipe({ optional: true })) page: number = 1,
    @Query('limit', new ParseIntPipe({ optional: true })) limit: number = 10,
  ): Promise<PaginatedData<GetBirdhousesResponse[]>> {
    const skip = (page - 1) * limit;

    const birdhouses = await this.birdhouseService.getAllBirdhouses(
      skip,
      limit,
    );

    const totalCount = await this.birdhouseService.countAllBirdhouses();

    return {
      total: totalCount,
      limit,
      data: birdhouses.map((birdhouse: BirdhouseDocument) => {
        const { totalBirds, totalEggs } =
          this.birdhouseService.calculateTotalBirdsAndEggs(birdhouse);

        return {
          id: birdhouse.ubid,
          birds: totalBirds,
          eggs: totalEggs,
          longitude: birdhouse.longitude,
          latitude: birdhouse.latitude,
          name: birdhouse.name,
        };
      }),
    };
  }

  /**
   * Retrieves paginated residency data for a specific birdhouse by its ID.
   * @param {string} id - The unique ID of the birdhouse.
   * @param {number} page - The page number (optional, default: 1).
   * @param {number} limit - The number of items per page (optional, default: 10).
   * @returns {Promise<PaginatedData<ResidencyData[]>>} A promise that resolves to paginated residency data.
   */
  @UseGuards(UbidGuard)
  @Get(':id/residency')
  async getPaginatedResidencyData(
    @Param('id') id: string,
    @Query('page', new ParseIntPipe({ optional: true })) page: number = 1,
    @Query('limit', new ParseIntPipe({ optional: true })) limit: number = 10,
  ): Promise<GetPaginatedResidencyDataResult> {
    return await this.birdhouseService.getPaginatedResidencyData({
      id,
      page,
      limit,
    });
  }

  /**
   * Retrieves graph residency data for a specific birdhouse by its ID.
   * @param {string} id - The unique ID of the birdhouse.
   * @returns {Promise<ResidencyData[]>} A promise that resolves to an array of graph residency data.
   */
  @Get(':id/graph-residency')
  async getGraphResidencyData(
    @Param('id') id: string,
  ): Promise<ResidencyData[]> {
    return await this.birdhouseService.getGraphResidencyData(id);
  }
}
