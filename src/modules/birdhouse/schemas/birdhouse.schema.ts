import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: 'birdhouses' })
export class BirdhouseDocument extends Document {
  @Prop({ isRequired: true, unique: true })
  ubid: string;

  @Prop({ isRequired: true })
  longitude: number;

  @Prop({ isRequired: true })
  latitude: number;

  @Prop({
    isRequired: true,
    minlength: 4,
    maxlength: 16,
    validate: {
      validator: (value: string) => {
        return value.length >= 4 && value.length <= 16;
      },
      message: 'Name must be between 4 and 16 characters long',
    },
  })
  name: string;

  @Prop({
    type: [
      {
        _id: false,
        birds: Number,
        eggs: Number,
        timestamp: Date,
      },
    ],
    default: [],
  })
  residencyData: {
    birds: number;
    eggs: number;
    timestamp: Date;
  }[];
}

export const BirdhouseSchema = SchemaFactory.createForClass(BirdhouseDocument);
