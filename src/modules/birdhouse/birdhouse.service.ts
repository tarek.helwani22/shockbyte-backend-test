import { Injectable, Logger } from '@nestjs/common';
import { MongoDataService } from '@/providers/database/mongoose/mongo-data.service';
import { Birdhouse } from './types/birdhouse.type';
import {
  ResidencyData,
  ResidencyDataWithTimestamp,
} from './types/residency-data.type';
import { BirdhouseDocument } from './schemas/birdhouse.schema';
import { v4 as uuidv4 } from 'uuid';
import { BirdhouseNotFoundException } from '@/modules/birdhouse/exceptions/birdhouse-not-found.exception';
import {
  GetPaginatedResidencyDataResult,
  GetPaginatedResidencyDataInput,
} from '@/modules/birdhouse/defs/birdhouse-service.defs';

@Injectable()
export class BirdhouseService {
  constructor(
    private readonly mongoDataServices: MongoDataService,
    private readonly logger: Logger,
  ) {}

  async createBirdhouse(birdhouseData: Birdhouse): Promise<BirdhouseDocument> {
    const ubid = uuidv4();

    await this.mongoDataServices.birdhouses.create({
      ...birdhouseData,
      ubid,
      birds: 0,
      eggs: 0,
    });

    this.logger.log(`Birdhouse created with UBID: ${ubid}`);

    return this.mongoDataServices.birdhouses.findOne({
      ubid,
    });
  }

  async updateBirdhouse(
    id: string,
    birdhouseData: Partial<Birdhouse>,
  ): Promise<BirdhouseDocument> {
    const birdhouse = await this.getBirdhouseInfo(id);

    if (!birdhouse) {
      throw new BirdhouseNotFoundException();
    }

    const updatedBirdhouse = await birdhouse
      .updateOne(birdhouseData, {
        new: true,
      })
      .exec();

    this.logger.log(
      `Birdhouse with UBID: ${updatedBirdhouse.ubid} has been updated`,
    );

    return updatedBirdhouse;
  }

  async addResidencyData(
    id: string,
    residencyData: ResidencyData,
  ): Promise<BirdhouseDocument> {
    const birdhouse = await this.getBirdhouseInfo(id);

    if (!birdhouse) {
      throw new BirdhouseNotFoundException();
    }

    birdhouse.residencyData.push({
      birds: residencyData.birds,
      eggs: residencyData.eggs,
      timestamp: new Date(),
    });

    const updatedBirdhouse = await birdhouse.save();
    this.logger.log(`Residency data added to birdhouse with ID ${id}.`);

    return updatedBirdhouse;
  }

  async getBirdhouseInfo(id: string): Promise<BirdhouseDocument> {
    const birdhouse = await this.findByUbid(id);

    if (!birdhouse) {
      throw new BirdhouseNotFoundException();
    }

    return birdhouse;
  }

  async getAllBirdhouses(
    skip: number,
    limit: number,
  ): Promise<BirdhouseDocument[]> {
    return this.mongoDataServices.birdhouses
      .find<BirdhouseDocument>()
      .skip(skip)
      .limit(limit)
      .exec();
  }

  async findByUbid(ubid: string): Promise<BirdhouseDocument | null> {
    return this.mongoDataServices.birdhouses.findOne({ ubid });
  }

  calculateTotalBirdsAndEggs(birdhouseDocument: BirdhouseDocument): {
    totalBirds: number;
    totalEggs: number;
  } {
    const totalBirds = birdhouseDocument.residencyData.reduce(
      (accumulator, entry) => accumulator + entry.birds,
      0,
    );

    const totalEggs = birdhouseDocument.residencyData.reduce(
      (accumulator, entry) => accumulator + entry.eggs,
      0,
    );

    return { totalBirds, totalEggs };
  }

  async countAllBirdhouses(): Promise<number> {
    return this.mongoDataServices.birdhouses.countDocuments();
  }

  async getPaginatedResidencyData(
    input: GetPaginatedResidencyDataInput,
  ): Promise<GetPaginatedResidencyDataResult> {
    const { id, limit, page } = input;

    this.logger.debug(
      `Fetching paginated residency data for birdhouse ID ${id}`,
    );

    const birdhouse = await this.getBirdhouseInfo(id);

    if (!birdhouse) {
      this.logger.warn(`Birdhouse with ID ${id} not found`);
      throw new BirdhouseNotFoundException();
    }

    const { residencyData } = birdhouse;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const paginatedData = residencyData.slice(startIndex, endIndex);

    this.logger.debug(`Paginated data retrieved for birdhouse ID ${id}`);

    return {
      total: residencyData.length,
      limit,
      data: paginatedData,
    };
  }

  async getGraphResidencyData(
    id: string,
  ): Promise<ResidencyDataWithTimestamp[]> {
    this.logger.debug(`Fetching graph residency data for birdhouse ID ${id}`);

    const birdhouse = await this.getBirdhouseInfo(id);

    if (!birdhouse) {
      this.logger.warn(`Birdhouse with ID ${id} not found`);
      throw new BirdhouseNotFoundException();
    }

    const { residencyData } = birdhouse;

    // Get the current week's start and end dates
    const currentDate = new Date();
    const currentDay = currentDate.getDay(); // 0 for Sunday, 1 for Monday, etc.
    const daysToSubtract = currentDay === 0 ? 6 : currentDay - 1; // Adjust for Monday as the first day
    const startDate = new Date(currentDate);
    startDate.setDate(currentDate.getDate() - daysToSubtract); // Start of the week
    const endDate = new Date(startDate);
    endDate.setDate(startDate.getDate() + 6); // End of the week

    const graphData: ResidencyDataWithTimestamp[] = [];

    for (
      let date = new Date(startDate);
      date <= endDate;
      date.setDate(date.getDate() + 1)
    ) {
      const day = date.getDate();
      const month = date.getMonth() + 1;
      const year = date.getFullYear();
      const dateString = `${year}-${month}-${day}`;

      // Filter residency data for the current day
      const dataForDay = residencyData.filter((entry) => {
        const entryDate = new Date(entry.timestamp);
        const entryDay = entryDate.getDate();
        const entryMonth = entryDate.getMonth() + 1;
        const entryYear = entryDate.getFullYear();
        const entryDateString = `${entryYear}-${entryMonth}-${entryDay}`;
        return entryDateString === dateString;
      });

      // Calculate total eggs and birds for the day
      let totalEggs = 0;
      let totalBirds = 0;

      dataForDay.forEach((entry) => {
        totalEggs += entry.eggs;
        totalBirds += entry.birds;
      });

      graphData.push({
        timestamp: new Date(dateString),
        eggs: totalEggs,
        birds: totalBirds,
      });
    }

    this.logger.debug(`Graph residency data retrieved for birdhouse ID ${id}`);

    return graphData;
  }
}
