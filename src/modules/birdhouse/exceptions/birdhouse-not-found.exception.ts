import { HttpStatus } from '@nestjs/common';
import { AppException } from '@/common/exceptions/app.exception';

export class BirdhouseNotFoundException extends AppException {
  constructor() {
    super({
      message: 'Birdhouse not found',
      code: 'NO_BIRDHOUSE_FOUND',
      statusCode: HttpStatus.NOT_FOUND,
    });
  }
}
