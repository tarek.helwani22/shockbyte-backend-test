import { Logger, Module } from '@nestjs/common';
import { MongooseProviderModule } from '../../providers/database/mongoose/mongoose-provider.module';
import { BirdhouseService } from './birdhouse.service';
import { BirdhouseController } from './birdhouse.controller';
import { UbidGuard } from './guards/ubid.guard';

@Module({
  imports: [MongooseProviderModule],
  providers: [BirdhouseService, Logger],
  controllers: [BirdhouseController],
  exports: [BirdhouseService],
})
export class BirdhouseModule {}
