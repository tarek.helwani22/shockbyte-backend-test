import { Module } from '@nestjs/common';
import { BirdhouseModule } from './birdhouse/birdhouse.module';

@Module({
  imports: [BirdhouseModule],
})
export class FeatureBundleModule {}
