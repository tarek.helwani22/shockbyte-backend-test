import { Injectable, Logger } from '@nestjs/common';
import { MongoDataService } from '@/providers/database/mongoose/mongo-data.service';
import { BirdhouseDocument } from '@/modules/birdhouse/schemas/birdhouse.schema';
import { birdhousesDataToSeed } from './birdhouse.data';
import { BirdhouseService } from '@/modules/birdhouse/birdhouse.service';
import { CreateBirdhouseDto } from '@/modules/birdhouse/dto/create-birdhouse.dto';

@Injectable()
export class BirdhouseSeederService {
  constructor(
    private readonly mongoDataServices: MongoDataService,
    private readonly birdhouseService: BirdhouseService,
    private readonly logger: Logger,
  ) {}

  async clearAndSeed(): Promise<Array<BirdhouseDocument>> {
    try {
      this.logger.debug('Clearing old birdhouses data..');

      await this.mongoDataServices.birdhouses.deleteMany({});

      const promises: Promise<BirdhouseDocument>[] = birdhousesDataToSeed.map(
        async (birdHouse: CreateBirdhouseDto) => {
          return await this.birdhouseService.createBirdhouse(birdHouse);
        },
      );

      return await Promise.all(promises);
    } catch (error) {
      throw error;
    }
  }
}
