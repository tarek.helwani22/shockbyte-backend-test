import { Logger, Module } from '@nestjs/common';
import { BirdhouseSeederService } from './birdhouse-seeder.service';
import { MongooseProviderModule } from '@/providers/database/mongoose/mongoose-provider.module';
import { BirdhouseModule } from '@/modules/birdhouse/birdhouse.module';

@Module({
  imports: [MongooseProviderModule, BirdhouseModule],
  providers: [BirdhouseSeederService, Logger],
  exports: [BirdhouseSeederService],
})
export class BirdhouseSeederModule {}
