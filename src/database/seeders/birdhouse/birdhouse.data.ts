import { CreateBirdhouseDto } from '@/modules/birdhouse/dto/create-birdhouse.dto';

export const birdhousesDataToSeed: CreateBirdhouseDto[] = [
  {
    longitude: 12.3456,
    latitude: 34.5678,
    name: 'Birdhouse 1',
  },
  {
    longitude: 45.6789,
    latitude: 56.789,
    name: 'Birdhouse 2',
  },
  {
    longitude: 78.9012,
    latitude: 90.1234,
    name: 'Birdhouse 3',
  },
  {
    longitude: 78.9012,
    latitude: 90.1234,
    name: 'Birdhouse 5',
  },
  {
    longitude: 78.9012,
    latitude: 90.1234,
    name: 'Birdhouse 6',
  },
  {
    longitude: 78.9012,
    latitude: 90.1234,
    name: 'Birdhouse 7',
  },
  {
    longitude: 78.9012,
    latitude: 90.1234,
    name: 'Birdhouse 8',
  },
];
