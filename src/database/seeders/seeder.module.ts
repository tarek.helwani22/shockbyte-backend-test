import { Logger, Module } from '@nestjs/common';
import { Seeder } from './seeder';
import { MongooseProviderModule } from '@/providers/database/mongoose/mongoose-provider.module';
import { BirdhouseSeederModule } from './birdhouse/birdhouse-seeder.module';

@Module({
  imports: [MongooseProviderModule, BirdhouseSeederModule],
  providers: [Logger, Seeder],
})
export class SeederModule {}
