import { Injectable, Logger } from '@nestjs/common';
import { BirdhouseSeederService } from './birdhouse/birdhouse-seeder.service';

@Injectable()
export class Seeder {
  constructor(
    private readonly logger: Logger,
    private readonly birdhouseSeederService: BirdhouseSeederService,
  ) {}

  async seed() {
    await this.birdhouses()
      .then((completed) => {
        this.logger.debug('Successfully completed seeding birdhouses...');
        Promise.resolve(completed);
      })
      .catch((error) => {
        this.logger.error('Failed seeding birdhouses...');
        Promise.reject(error);
      });
  }

  async birdhouses() {
    return await Promise.all(await this.birdhouseSeederService.clearAndSeed())
      .then((createdBirdhouses) => {
        this.logger.debug(
          'Num of birdhouses created : ' + createdBirdhouses.length,
        );
        return Promise.resolve(true);
      })
      .catch((error) => Promise.reject(error));
  }
}
