import { Module } from '@nestjs/common';
import { ConfigsModule } from './config/configs.module';
import { FeatureBundleModule } from './modules/feature-bundle.module';
import { ProvidersModule } from './providers/providers.module';
import { CronModule } from './jobs/cron/cron.module';
import { APP_FILTER } from '@nestjs/core';
import { GlobalExceptionFilter } from './common/filter/global-exception.filter';

@Module({
  imports: [
    ConfigsModule,
    FeatureBundleModule,
    ProvidersModule,
    FeatureBundleModule,
    CronModule,
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: GlobalExceptionFilter,
    },
  ],
})
export class AppModule {}
