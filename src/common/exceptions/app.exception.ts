import { IBaseException } from '../interfaces/exceptions/base-exception.interface';
import { HttpStatus } from '@nestjs/common';

export interface IAppException {
  message?: string;
  code?: string;
  statusCode?: HttpStatus;
}

export class AppException extends Error implements IBaseException {
  code: string;
  statusCode: number;

  constructor(data: IAppException) {
    const { message, code, statusCode } = data;

    super(message);
    this.code = code;
    this.statusCode = statusCode;
  }
}
