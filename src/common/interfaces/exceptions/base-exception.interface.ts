export interface IBaseException {
  code: string;
  statusCode: number;
}
