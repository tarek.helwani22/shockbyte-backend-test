import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Injectable,
  NotImplementedException,
} from '@nestjs/common';
import { AppException } from '../exceptions/app.exception';
import { AppConfigService } from '@/config/app/app-config.service';
import { EnvTypes } from '@/config/app/types/enviroment';
import {
  getRequestFromHost,
  getRequestTypeFromHost,
  RequestType,
} from '../helpers/request.helper';

@Catch()
@Injectable()
export class GlobalExceptionFilter implements ExceptionFilter {
  constructor(private readonly appConfigService: AppConfigService) {}

  catch(exception: any, host: ArgumentsHost): any {
    let generalTypeException: GeneralTypeException;

    if (exception instanceof AppException) {
      generalTypeException = new GeneralTypeException({
        message: exception.message,
        code: exception.code,
        statusCode: exception.statusCode,
        stack: exception.stack,
      });
    } else if (exception instanceof HttpException) {
      const response: string | Record<string, any> = exception.getResponse();

      let message: string;
      if (typeof response === 'string') {
        message = response;
      } else if (typeof response === 'object' && response.message) {
        message = response.message;
      } else {
        message = 'An unknown error occurred';
      }

      generalTypeException = new GeneralTypeException({
        message,
        stack: exception.stack,
        statusCode: exception.getStatus() ?? HttpStatus.INTERNAL_SERVER_ERROR,
      });
    }

    if (this.appConfigService.env === EnvTypes.DEVELOPMENT)
      switch (getRequestTypeFromHost(host)) {
        case RequestType.HTTP:
          return this.httpExceptionHandler(generalTypeException, host);
      }

    throw new NotImplementedException(
      'Unable to identify user request destination',
    );
  }

  httpExceptionHandler(
    generalTypeException: GeneralTypeException,
    host: ArgumentsHost,
  ) {
    const response = getRequestFromHost(host).res;
    return response.status(generalTypeException.statusCode).json({
      message: generalTypeException.message,
      statusCode: generalTypeException.statusCode,
      stack: generalTypeException.stack,
    });
  }
}

export interface IGeneralTypeException {
  message: string;
  code?: string;
  statusCode?: number;
  stack?: string;
}

class GeneralTypeException extends Error {
  message: string;
  code: string;
  statusCode: number;
  stack: string;

  constructor(data: IGeneralTypeException) {
    const { message, code, statusCode, stack } = data;

    super(message);
    this.message = message;
    this.code = code;
    this.statusCode = statusCode;
    this.stack = stack;
  }
}
