export interface PaginatedData<T> {
  total: number;
  limit: number;
  data: T;
}
