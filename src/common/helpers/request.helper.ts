import { ArgumentsHost } from '@nestjs/common';
import { Request } from 'express';

export class RequestType {
  static readonly HTTP = 'http';
}

export function getRequestTypeFromHost(host: ArgumentsHost): RequestType {
  return host.getType();
}

export function getRequestFromHost(host: ArgumentsHost): Request {
  if (getRequestTypeFromHost(host) === RequestType.HTTP) {
    return host.switchToHttp().getRequest();
  }
}
